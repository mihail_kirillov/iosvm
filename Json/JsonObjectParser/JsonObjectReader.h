//
//  JsonObjectReader.h
//  JSONObject
//
//  Created by Alexander Kryshtalev on 24.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JsonObjectReader : NSObject

+ (id) readObjectFromJson: (NSString*) json forClass: (id) _class;

+ (id) readObjectFromJson: (NSString*) json forClass: (id) _class withNode: (NSString*) withNode;

+ (id) readObjectFromDictionary: (NSDictionary*) dictionary forClass: (id) _class withNode: (NSString*) withNode;

+ (id) readObjectFromDictionary: (NSDictionary*) dictionary forClass: (id) _class;

@end
