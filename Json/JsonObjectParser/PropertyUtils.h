//
//  PropertyUtils.h
//  JSONObject
//
//  Created by Alexander Kryshtalev on 24.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PropertyUtils : NSObject

// + (NSDictionary *)classPropsFor:(Class)_class;
+ (NSDictionary *)classAndParentPropsFor:(Class)klass;
+ (NSArray *)propertyNamesForClass:(Class)klass;

@end
