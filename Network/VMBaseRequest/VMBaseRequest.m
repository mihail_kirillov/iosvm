//
//  VMBaseRequest.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMBaseRequest.h"

@implementation VMBaseRequest

@synthesize method;

- (id)init
{
    self = [super init];
    if (self){
        NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
        self.version = [NSString stringWithFormat:@"Version %@ (%@)", version, build];
        self.bundle_id = [[NSBundle mainBundle] bundleIdentifier];
    }
    return self;
}

- (id)initWithMethod:(NSString *)_method;
{
	self = [self init];
	self.method = _method;
	return self;
}

- (NSString *)getUrl
{
	NSString *current = self.method;
 	if (current != nil && current.length != 0) {
        return [baseUrl stringByAppendingString:current];
    } else {
 		NSException *exception = [NSException exceptionWithName: @"EmptyMethodException"
 														 reason: @"This method must be overridden"
 													   userInfo: nil];
 		@throw exception;
 	}
}

@end
