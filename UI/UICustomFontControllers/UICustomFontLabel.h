//
//  UICustomFontLabel.h
//  NDA
//
//  Created by Alexander Kryshtalev on 19.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UICustomFontLabel : UILabel

@property (nonatomic) NSString *customFont;

@end
