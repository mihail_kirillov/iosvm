//
//  VMNavBarButton.m
//  GSPrice
//
//  Created by Alexander Kryshtalev on 28.09.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMNavBarButton.h"

#define OFFSET 8

@implementation VMNavBarButton

- (id)init
{
	self = [super init];
	if (self) {
		self.ios7offset = OFFSET;
		self.ios6offset = OFFSET;
    }
	return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		self.ios7offset = OFFSET;
		self.ios6offset = OFFSET;
    }
    return self;
}

- (void)awakeFromNib
{
	self.ios7offset = OFFSET;
	self.ios6offset = OFFSET;
}

- (UIEdgeInsets)alignmentRectInsets {
    UIEdgeInsets insets;
    if ([[UIDevice currentDevice] systemVersion].floatValue >= 7.0) {
        if ([self isLeftButton]) {
            insets = UIEdgeInsetsMake(0, self.ios7offset, 0, 0);
        } else {
            insets = UIEdgeInsetsMake(0, 0, 0, self.ios7offset);
        }
    } else {
        if ([self isLeftButton]) {
            insets = UIEdgeInsetsMake(0, -self.ios6offset, 0, 0);
        } else {
            insets = UIEdgeInsetsMake(0, 0, 0, -self.ios6offset);
        }

    }
	
    return insets;
}

- (BOOL)isLeftButton {
    return self.frame.origin.x < (self.superview.frame.size.width / 2);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
