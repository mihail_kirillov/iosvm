//
//  TabbarControllerViewController.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 19.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMTabbarItem.h"

@interface VMTabbarViewController : UIViewController
{
	VMTabbarItem* selected;
}

- (void)addTabbarItemForController: (id) controllerClass normalImage: (UIImage*) normalImage selectedImage: (UIImage*) selectedImage;
- (void)addTabbarItemForController: (id) controllerClass normalImageName: (NSString*) normalImage selectedImageName: (NSString*) selectedImage;

- (void)addTabbarItemForController: (id) controllerClass normalImage: (UIImage*) normalImage selectedImage: (UIImage*) selectedImage withTitle:(NSString *)title;
- (void)addTabbarItemForController: (id) controllerClass normalImageName: (NSString*) normalImage selectedImageName: (NSString*) selectedImage withTitle:(NSString *)title;

- (void)selectTabAtIndex: (NSUInteger) index animated:(BOOL) animated;
- (CGRect)clientFrame;
- (int)realTabbarHeight;

- (UINavigationController *) navigationControllerAtIndex: (NSUInteger) index;

@property (retain) NSMutableArray* items;

@property BOOL popToRootOnClick;
@property int tabbarHeight;
@property BOOL autosizeTabbar;
@property BOOL tabbarHidden;
@property BOOL resizeClientFrame;

@property (copy) UIFont *titleFont;
@property (copy) UIColor *normalTitleColor, *selectedTitleColor;

@end
