//
//  TabbarItem.h
//  RippIn
//
//  Created by Alexander Kryshtalev on 19.09.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VMTabbarViewController;

@interface VMTabbarItem : NSObject
{
	UINavigationController* navController;
	UIViewController* controller;
	id controllerClass;
	UIImage *currentImage;
}

@property (retain) UIButton* button;
@property (retain) UIImage* normalImage;
@property (retain) UIImage* selectedImage;

@property (retain) UILabel* label;
@property (retain) UIColor* selectedTitleColor, *normalTitleColor;
@property (retain) NSString* title;

- (void) setSelected: (bool) selected;
- (id) initForTabbarController: (VMTabbarViewController*) tabbarController
			   controllerClass: (id) controllerClass
				   normalImage: (UIImage*) normalImage
				 selectedImage: (UIImage*) selectedImage;

- (id) initForTabbarController: (VMTabbarViewController*) tabbarController
			   controllerClass: (id) controllerClass
				   normalImage: (UIImage*) normalImage
				 selectedImage: (UIImage*) selectedImage
                         title: (NSString*) title
              normalTitleColor: (UIColor*) normalColor
            selectedTitleColor: (UIColor*) selectedColor;

- (UINavigationController*) getController;

@end
