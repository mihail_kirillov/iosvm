//
//  VMRequestViewController.m
//  RippIn
//
//  Created by Alexander Kryshtalev on 06.12.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMRequestViewController.h"

@implementation VMRequestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	requests = [[NSMutableArray alloc] init];
}

- (void)viewWillDisappear:(BOOL)animated {
	[self cancelRequests];
}

- (void)cancelRequests
{
	if (requests && requests.count > 0) {
		for (ASIHTTPRequest *request in requests) {
			[request clearDelegatesAndCancel];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(ASIHTTPRequest *)createRequestForJsonObject:(VMBaseRequest *)jsonBody
{
    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[jsonBody getUrl]]];
	request.timeOutSeconds = 60 * 10; // timeout is 10 minutes
	[request setRequestMethod:@"post"];
	request.userInfo = [[NSDictionary alloc] initWithObjectsAndKeys:self, @"self_instance", nil];
	
	SBJsonWriter *writer = [[SBJsonWriter alloc] init];
	NSDictionary *dict = [JsonObjectWriter dictionaryFromObject:jsonBody withRoot:@"request"];
	NSString *jsonString = [writer stringWithObject: dict];
	NSLog(@"Request URL: %@",[jsonBody getUrl]);
	NSLog(@"Request body: %@", jsonString);
	
	request.postBody = [[NSMutableData alloc] initWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
	[requests addObject:request];
	return request;
}

- (id)createResponseForRequest:(ASIHTTPRequest *)request forClass:(id)theClass
{
    NSLog(@"Request method: %@", request.url);
	NSLog(@"Response body: %@", request.responseString);
	
	SBJsonParser *parser = [[SBJsonParser alloc] init];
	id result = [parser objectWithString:request.responseString];
	id response = [JsonObjectReader readObjectFromDictionary:(NSDictionary *)result forClass:theClass withNode:@"response"];
	return response;
}

- (void)executeRequest:(id)requestClass withMethod:(NSString *)method forResponse:(id)responseClass completion:(RequestDoneBlock)completionBlock
{
    VMBaseRequest *request = [[VMBaseRequest alloc] init];
    request.method = method;
    [self executeRequestWithBody:request forResponse:responseClass completion:completionBlock];
}

- (void)executeRequestWithBody:(VMBaseRequest *)post forResponse:(id)responseClass completion:(RequestDoneBlock)completionBlock
{
    ASIHTTPRequest *httpRequest = [self createRequestForJsonObject:post];
	__weak ASIHTTPRequest *weakRequest = httpRequest;
	[httpRequest setCompletionBlock: ^{
		completionBlock([self createResponseForRequest:weakRequest forClass:responseClass]);
	}];
    [httpRequest setFailedBlock:^{
        completionBlock(nil);
    }];
	[httpRequest startAsynchronous];
}

@end
