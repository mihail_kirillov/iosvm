//
//  EmptyValidator.m
//  BetYou
//
//  Created by Alexander Kryshtalev on 19.11.12.
//  Copyright (c) 2012 Voodoo Mobile. All rights reserved.
//

#import "VMEmptyValidator.h"

@implementation VMEmptyValidator


- (BOOL)validateInput:(id)input
{
	NSString *value = nil;
	if ([input isKindOfClass:[UITextField class]]) {
		value = ((UITextField *)input).text ;
		
	} else if ([input isKindOfClass:[UITextView class]]) {
		value = ((UITextField *)input).text ;
		
	} else {
		return TRUE;
	}
	
	return value != nil && value.length != 0;
}

@end
