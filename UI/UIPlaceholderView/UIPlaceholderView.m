//
//  UIPlaceholderView.m
//  NDA
//
//  Created by Alexander Kryshtalev on 19.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "UIPlaceholderView.h"

@implementation UIPlaceholderView

@synthesize replacementController;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSString *)replacementController;
{
	return replacementController;
}

- (void)setReplacementController:(NSString *)_replacementController
{
	replacementController = _replacementController;
	[self placeViewController ];
}

- (void)awakeFromNib
{
	[self placeViewController ];
}

- (void)placeViewController
{
	if (self.controllerInstance) {
		[self.controllerInstance.view removeFromSuperview];
		self.controllerInstance = nil;
	}
	
	self.controllerInstance = [[NSClassFromString(self.replacementController) alloc] initWithNibName:self.replacementController bundle:nil];
	self.controllerInstance.view.frame = self.bounds;
	[self addSubview:self.controllerInstance.view];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
