//
//  AuxManager.m
//  Prodensa
//
//  Created by Alexander Kryshtalev on 01.02.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "AuxManager.h"
#import "AppDelegate.h"

@implementation AuxManager

+ (void)callPhoneNumber:(NSString *)number
{
	NSString *formatted = [number stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [number length])];
    NSURL* callUrl=[NSURL URLWithString:[NSString   stringWithFormat:@"tel:+%@", formatted]];
	
	//check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:callUrl])
    {
        [[UIApplication sharedApplication] openURL:callUrl];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Info" message:@"This function is only available on the iPhone"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
}

+ (UIViewController *)rootController
{
    return ((AppDelegate *)[UIApplication sharedApplication].delegate).window.rootViewController;
}

+ (MFMailComposeViewController *)composeEmailTo:(NSString *)recipient withSubject:(NSString *)subject andMessage:(NSString *)message
{
	if ([MFMailComposeViewController canSendMail]) {
		UIViewController *rootController = [AuxManager rootController];
		MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
		[controller setSubject:subject];
		[controller setToRecipients:[[NSArray alloc] initWithObjects:recipient, nil]];
		[controller setMessageBody:message isHTML:FALSE];
		[rootController presentModalViewController:controller animated:YES];
		
		return controller;
	} 
	
	return nil;
}

+ (MFMessageComposeViewController *)composeMessageTo:(NSString *)recipient withMessage:(NSString *)message
{
    if ([MFMessageComposeViewController canSendText]) {
        UIViewController *rootController = [AuxManager rootController];
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        controller.messageComposeDelegate = nil;
        controller.body = message;
        controller.recipients = [NSArray arrayWithObjects:recipient, nil];
        [rootController presentViewController:controller animated:NO completion:nil];
    
        return controller;
    }
    return nil;
}

@end
