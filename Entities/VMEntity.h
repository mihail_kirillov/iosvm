//
//  VMEntity.h
//  NDA
//
//  Created by Alexander Kryshtalev on 22.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JsonDataProvider.h"

@interface VMEntity : NSObject <NSCopying, JsonDataProvider>

+ (id)objectFromDictionary:(NSDictionary *)dictionary;
- (NSDictionary *)dictionaryRepresentation;

@end
