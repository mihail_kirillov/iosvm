//
//  VMEntity.m
//  NDA
//
//  Created by Alexander Kryshtalev on 22.01.13.
//  Copyright (c) 2013 Voodoo Mobile. All rights reserved.
//

#import "VMEntity.h"
#import "PropertyUtils.h"
#import "JsonObjectReader.h"
#import "JsonObjectWriter.h"

@implementation VMEntity

- (id)copyWithZone:(NSZone *)zone
{
    id newEntity = [[[self class] allocWithZone:zone] init];
    NSDictionary *props = [PropertyUtils classAndParentPropsFor:[self class]];
	for (NSString *propertyName in [props allKeys]) {
        id value = [self valueForKey:propertyName];
        [newEntity setValue:[value copyWithZone:zone] forKey:propertyName];
    }
    return newEntity;
}

- (BOOL)isEqual:(id)object
{
    NSDictionary *props = [PropertyUtils classAndParentPropsFor:[self class]];
	for (NSString* propertyName in [props allKeys]) {
        
        id obj1 = [self valueForKey:propertyName];
        id obj2 = [object valueForKey:propertyName];
        
        if (obj1 != nil || obj2 != nil){
            if (![obj1 isEqual:obj2]){
                return NO;
            }
        }
    }
    return YES;
}

- (id)arrayClassForProperty:(NSString *)propertyName
{
    return nil;
}

- (NSDictionary *)dictionaryRepresentation
{
    return [JsonObjectWriter dictionaryFromObject:self];
}

+ (id)objectFromDictionary:(NSDictionary *)dictionary
{
    return [JsonObjectReader readObjectFromDictionary:dictionary forClass:[self class]];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
	
}

@end
